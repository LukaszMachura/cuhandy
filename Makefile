#CC = nvcc
CC = /usr/local/cuda/bin/nvcc
CFLAGS =-arch=sm_13 -m64 --use_fast_math -O3
CURAND =-L/usr/local/cuda/lib64 -lcurand

all: cuhandy 

cuhandy: cuhandy.cu
	$(CC) --gpu-architecture=compute_35 -o cuhandy cuhandy.cu $(CURAND) -lm


.PHONY: clean mrproper test

clean: 
	rm -rf *.bin cuhandy

test:
	./cuhandy --betaC=0.03 --betaE=0.03 --lambda=100 --delta=0.00000677 --gamma=0.01 --noise=0 --paths=1 --trans=0 --spp=365 --years=200 --block=1 --dev=0 --ro=0.005 --kappa=1 --s=0.0005 --alpham=0.01 --alphaM=0.07 --xC_init=1000 --xE_init=25 --y_init=100 --w_init=0
