#!/usr/bin/python3
import subprocess

betaC = "--betaC=%f" % 0.03
betaE = "--betaE=%f" % 0.03
lambda2 = "--lambda=%f" % 100
delta = "--delta=%f" % 0.0000267
gamma = "--gamma=%f" % 0.01
noise = "--noise=%f" % 0 
paths = "--paths=%f" % 1 
trans = "--trans=%f" % 0 
spp = "--spp=%f" % 730 
years = "--years=%f" % 10000 
block = "--block=%f" % 1 
dev = "--dev=%f" % 0 
ro = "--ro=%f" % 0.005 
kappa = "--kappa=%f" % 1 
s = "--s=%f" % 0.0005 
alpham = "--alpham=%f" % 0.01 
alphaM ="--alphaM=%f" % 0.07
xC_init = "--xC_init=%f" % 100
xE_init = "--xE_init=%f" % 0
y_init = "--y_init=%f" % 100
w_init = "--w_init=%f" % 0
outfile = 'test123.dat'
#...
datatxt = subprocess.getoutput("./cuhandy {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {}".format(betaC, betaE, lambda2, delta, gamma, noise, paths, trans, spp, years, block, dev, ro, kappa, s, alpham, alphaM, xC_init, xE_init, y_init, w_init))

print(datatxt[:333])
# data = pd.compat.StringIO(datatxt)
# df = pd.read_csv(data, sep=" ")