/*
 * CUDA dynamics for stochastic HANDY model
 * S. Motesharrei et al. / Ecological Economics 101 (2014) 90–102
 */

#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <cuda.h>
#include <curand.h>
#include <curand_kernel.h>

#define PI 3.14159265358979f

#define checkpoint(a) { fprintf(stderr, "CHECK %c\n", char(a)); fflush(stderr); }

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
  if (code != cudaSuccess){
    fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
    if (abort) exit(code);
  }
}



//SHANDY
__constant__ float d_alphaC, d_betaC, 
                   d_betaE, d_alphaE, 
                   d_gamma, d_lambda, 
                   d_delta, d_CC, 
                   d_CE, d_noise,
		   d_ro, d_kappa,
                   d_S, d_alpham,
		   d_alphaM;
//vector
float *h_xC, *h_xE, *h_y, *h_w;  //, *h_wth, *h_CC, *h_CE, *h_alphaC, *h_alphaE;
float h_xCinit; //, *h_xE, *h_y, *h_w;  //, *h_wth, *h_CC, *h_CE, *h_alphaC, *h_alphaE;

float *d_xC, *d_xE, *d_y, *d_w;  //, *d_wth, *d_CC, *d_CE, *d_alphaC, *d_alphaE;

//simulation
float h_trans, h_xC_init, h_xE_init, h_y_init, h_w_init;
int h_dev, h_block, h_grid, h_spp;
long h_paths, h_years, h_threads, h_steps;
__constant__ int d_spp;
__constant__ long d_paths, d_steps;

unsigned int *h_seeds, *d_seeds;
curandState *d_states;

size_t size_f, size_ui, size_p;
curandGenerator_t gen;

static struct option options[] = {
    {"xC_init", required_argument, NULL, 'a'},
    {"xE_init", required_argument, NULL, 'b'},
    {"y_init", required_argument, NULL, 'c'},
    {"w_init", required_argument, NULL, 'd'},
    {"betaC", required_argument, NULL, 'e'},
    {"betaE", required_argument, NULL, 'f'},
    {"gamma", required_argument, NULL, 'g'},
    {"delta", required_argument, NULL, 'h'},
    {"lambda", required_argument, NULL, 'i'},
    {"noise", required_argument, NULL, 'j'},
    {"ro", required_argument, NULL, 'k'},
    {"kappa", required_argument, NULL, 'l'},
    {"s", required_argument, NULL, 'm'},
    {"alphaM", required_argument, NULL, 'n'},
    {"alpham", required_argument, NULL, 'o'},
    {"paths", required_argument, NULL, 'p'},
    {"trans", required_argument, NULL, 'r'},
    {"spp", required_argument, NULL, 's'},
    {"dev", required_argument, NULL, 't'},
    {"block", required_argument, NULL, 'w'},
    {"years", required_argument, NULL, 'x'},
};

void usage(char **argv)
{
    printf("Usage: %s <params> \n\n", argv[0]);
    printf("Model params:\n");
    printf("    -a, --xC_init=FLOAT     Population of commoner FLOAT\n");
    printf("    -b, --xE_init=FLOAT     Population of elite FLOAT\n");
    printf("    -c, --y_init=FLOAT      Nature at start FLOAT\n");
    printf("    -d, --w_init=FLOAT      Storage at start FLOAT\n");
    printf("    -e, --betaC=FLOAT       Commoner bith rate\\omega' FLOAT\n");
    printf("    -f, --betaE=FLOAT       Elite birth rate FLOAT\n");
    printf("    -g, --gamma=FLOAT       Regeneration rate of nature FLOAT\n\n");
    printf("    -h, --delta=FLOAT       Depletion (production) factor - set NONE - FLOAT\n");
    printf("    -i, --lambda=FLOAT      Nature carrying capacity FLOAT\n");
    printf("    -j, --noise=FLOAT       Noise intensity FLOAT\n\n");
    printf("    -k, --ro=FLOAT          Ro FLOAT\n\n");
    printf("    -l, --kappa=FLOAT       Kappa FLOAT\n\n");
    printf("    -m, --s=FLOAT           S FLOAT\n\n");
    printf("    -n, --alphaM=FLOAT      Alphamm FLOAT\n\n");
    printf("    -o, --alpham=FLOAT      Alpham FLOAT\n\n");
    printf("Simulation params:\n");
    printf("    -p, --paths=LONG        set the number of paths to LONG\n");
    printf("    -r, --trans=FLOAT       specify fraction FLOAT of periods which stands for transients\n");
    printf("    -s, --spp=INT           specify how many integration steps should be calculated \n");
    printf("                            per unit time\n\n");
    printf("    -t, --dev=INT           specify device number to INT \n");
    printf("    -w, --block=INT         set the gpu block size to INT\n");
    printf("    -x, --years=LONG        set the number of years to LONG\n");
    printf("\n");
}



void parse_cla(int argc, char **argv)
{
    float ftmp;
    int c, itmp;

    while( (c = getopt_long(argc, argv, "a:b:c:d:e:f:g:h:i:j:k:l:m:n:o:p:r:s:t:w:x", options, NULL)) != EOF) {
        switch (c) {
	    case 'a':
	        h_xC_init = atof(optarg);
                //checkpoint(c);
                break;
 	    case 'b':
                h_xE_init = atof(optarg);
                //checkpoint(c);
                break;
	    case 'c':
                h_y_init = atof(optarg);
                //checkpoint(c);
                break;
	    case 'd':
                h_w_init = atof(optarg);
                //checkpoint(c);
                break;
            case 'e':
                ftmp = atof(optarg);
                cudaMemcpyToSymbol(d_betaC, &ftmp, sizeof(float));
		//checkpoint(c);
                break;
            case 'f':
                ftmp = atof(optarg);
                cudaMemcpyToSymbol(d_betaE, &ftmp, sizeof(float));
		//checkpoint(c);
                break;
            case 'g':
                ftmp = atof(optarg);
                cudaMemcpyToSymbol(d_gamma, &ftmp, sizeof(float));
		//checkpoint(c);
                break;
            case 'h':
                ftmp = atof(optarg);
                cudaMemcpyToSymbol(d_delta, &ftmp, sizeof(float));
		//checkpoint(c);
                break;
            case 'i':
                ftmp = atof(optarg);
                cudaMemcpyToSymbol(d_lambda, &ftmp, sizeof(float));
		//checkpoint(c);
                break;
            case 'j':
		ftmp = atof(optarg);
                cudaMemcpyToSymbol(d_noise, &ftmp, sizeof(float));
                //checkpoint(c);
                break;
            case 'k':
                ftmp = atof(optarg);
                gpuErrchk(cudaMemcpyToSymbol(d_ro, &ftmp, sizeof(float)));
                //checkpoint(c);
                break;
            case 'l':
                ftmp = atof(optarg);
                gpuErrchk(cudaMemcpyToSymbol(d_kappa, &ftmp, sizeof(float)));
                //checkpoint(c);
                break;
            case 'm':
                ftmp = atof(optarg);
                gpuErrchk(cudaMemcpyToSymbol(d_S, &ftmp, sizeof(float)));
                //checkpoint(c);
                break;
            case 'n':
                ftmp = atof(optarg);
                gpuErrchk(cudaMemcpyToSymbol(d_alphaM, &ftmp, sizeof(float)));
                //checkpoint(c);
                break;
            case 'o':
                ftmp = atof(optarg);
                gpuErrchk(cudaMemcpyToSymbol(d_alpham, &ftmp, sizeof(float)));
                //checkpoint(c);
                break;
            case 'p':
                h_paths = atol(optarg);
                cudaMemcpyToSymbol(d_paths, &h_paths, sizeof(long));
                //checkpoint(c);                
	    case 'r':
                h_trans = atof(optarg);
                //checkpoint(c);
                break;
            case 's':
                h_spp = atoi(optarg);
                cudaMemcpyToSymbol(d_spp, &h_spp, sizeof(int));
                //checkpoint(c);
                break;
	    case 't':
                itmp = atoi(optarg);
                cudaSetDevice(itmp);
                //checkpoint(c);
                break;
            case 'w':
                h_block = atoi(optarg);
                //checkpoint(c);
                break;              
            case 'x':
		h_years = atol(optarg);
                //checkpoint(c);
                break;

                
            
                        
        }
    }
}


#ifndef __CU_RNG_H
#define __CU_RNG_H

#define PI 3.14159265358979f

/*
 * Return a uniformly distributed random number from the
 * [0;1] range.
 */
__device__ float rng_uni(unsigned int *state)
{
	unsigned int x = *state;

	x = x ^ (x >> 13);
	x = x ^ (x << 17);
	x = x ^ (x >> 5);

	*state = x;

	return x / 4294967296.0f;
}

/*
 * Generate two normal variates given two uniform variates.
 */
__device__ void bm_trans(float& u1, float& u2)
{
	float r = sqrtf(-2.0f * logf(u1));
	float phi = 2.0f * PI * u2;
	u1 = r * cosf(phi);
	u2 = r * sinf(phi);
}

#endif /* __CU_RNG_H */


__global__ 
void init_dev_rng(unsigned int *d_seeds, curandState *d_states)
{
    long idx = blockIdx.x * blockDim.x + threadIdx.x;

    curand_init(d_seeds[idx], idx, 0, &d_states[idx]);
}

__device__ 
void shandy(float &nl_xC, float l_xC, float &nl_xE, float l_xE, float &nl_y, float l_y, float &nl_w, float l_w, 
            curandState *l_state,
            float l_betaC, float l_betaE, 
	    float l_gamma, float l_delta, 
	    float l_lambda, 
	    float l_noise, 
            float l_dt, float l_ro, float l_kappa, float l_S, float l_alphaM, float l_alpham) 
{
    float l_xCt, l_xEt, l_yt, l_wt, l_wth, l_CC, l_CE, l_alphaC, l_alphaE;

    //tu 5 rownan
    l_wth = l_ro * l_xC + l_kappa *l_ro * l_xE;

    
    l_CC = fminf(1, l_w / l_wth) * l_S * l_xC;
    l_CE = fminf(1, l_w / l_wth) * l_S * l_kappa * l_xE;
    l_alphaC = l_alpham + fmaxf(0 , 1 - l_CC / l_S / l_xC) * (l_alphaM - l_alpham);
    l_alphaE = l_alpham + fmaxf(0 , 1 - l_CE / l_S / l_xE) * (l_alphaM - l_alpham); 


    // dynamics
    /*
    l_xCt = l_xC + (l_betaC * l_xC - l_alphaC * l_xC) * l_dt;
    l_xEt = l_xE + (l_betaE * l_xE - l_alphaE * l_xE) * l_dt;	//dopisane
    l_yt  = l_y + (l_gamma * l_y * (l_lambda - l_y) - l_delta * l_xC * l_y) * l_dt;
	*/


    l_xCt = l_xC * (1 + (l_betaC - l_alphaC) * l_dt);

    l_xEt = l_xE * (1 + (l_betaE - l_alphaE) * l_dt);	//dopisane

    l_yt  = l_y * (1 + (l_gamma * (l_lambda - l_y) - l_delta * l_xC) * l_dt);
    
    if ((l_yt > 0.0f) and (l_noise > 0.0f)){
      l_yt += sqrtf(2 * l_dt * l_noise) * curand_normal(l_state); //dopisane	
    }
    else{
      l_yt += 1.0e-15f;
    }
    if (l_yt < 0.0f){
	    l_yt = 1.0e-15f;
    }

    l_wt  = l_w + (l_delta * l_xC * l_y - l_CE - l_CC) * l_dt;	//dopisane
        
    nl_xC = l_xCt;
    nl_xE = l_xEt;
    nl_y = l_yt;
    nl_w = l_wt;    
}

__global__ void run_traj(float *d_xC, float *d_xE, float *d_y, float *d_w, curandState *d_states)
//actual trajectory kernel
{
    long idx = blockIdx.x * blockDim.x + threadIdx.x;
    float l_xC, l_xE, l_w, l_y; 
    curandState l_state;

    //cache path and model parameters in local variables
    l_xC = d_xC[idx];
    l_xE = d_xE[idx];
    l_y = d_y[idx];
    l_w = d_w[idx];
    l_state = d_states[idx];

    float 
      l_betaC = d_betaC, 
      l_betaE = d_betaE, 
      l_gamma = d_gamma, 
      l_delta = d_delta, 
      l_lambda = d_lambda, 
      l_noise = d_noise,
      l_ro = d_ro,
      l_kappa = d_kappa,
      l_S = d_S,
      l_alphaM = d_alphaM,
      l_alpham = d_alpham;

    //step size & number of steps
    float l_dt;
    long l_steps, i;

    l_dt = 1.0f / d_spp; 
    l_steps = d_steps;

    for (i = 0; i < l_steps; i++){
        shandy(l_xC, l_xC, l_xE, l_xE, l_y, l_y, l_w, l_w, 
            &l_state,
            l_betaC, l_betaE, 
	    l_gamma, l_delta, 
	    l_lambda, 
	    l_noise,  
            l_dt,
	    l_ro, l_kappa, l_S,
	    l_alphaM, l_alpham);
    }

    //write back path parameters to the global memory
    d_xC[idx] = l_xC;
    d_xE[idx] = l_xE;
    d_y[idx] = l_y;
    d_w[idx] = l_w;
    d_states[idx] = l_state;
}

void prepare()
//prepare simulation
{
    //grid size
    h_paths = (h_paths / h_block) * h_block;
    h_threads = h_paths;
    h_grid = h_threads / h_block;

    h_steps = h_spp;
    cudaMemcpyToSymbol(d_steps, &h_steps, sizeof(long));
     
    //host memory allocation
    size_f = h_threads * sizeof(float);
    size_ui = h_threads * sizeof(unsigned int);

    h_xC = (float*)malloc(size_f);
    h_xE = (float*)malloc(size_f);
    h_y = (float*)malloc(size_f);
    h_w = (float*)malloc(size_f);
    h_seeds = (unsigned int*)malloc(size_ui);

    //create & initialize host rng
    curandCreateGeneratorHost(&gen, CURAND_RNG_PSEUDO_DEFAULT);
    curandSetPseudoRandomGeneratorSeed(gen, time(NULL));

    curandGenerate(gen, h_seeds, h_threads);
 
    //device memory allocation
    cudaMalloc((void**)&d_xC, size_f);
    cudaMalloc((void**)&d_xE, size_f);
    cudaMalloc((void**)&d_y, size_f);
    cudaMalloc((void**)&d_w, size_f);
    cudaMalloc((void**)&d_seeds, size_ui);
    cudaMalloc((void**)&d_states, h_threads*sizeof(curandState));

    //copy seeds from host to device
    cudaMemcpy(d_seeds, h_seeds, size_ui, cudaMemcpyHostToDevice);

    //initialization of device rng
    init_dev_rng<<<h_grid, h_block>>>(d_seeds, d_states);

    free(h_seeds);
    cudaFree(d_seeds);
}

void copy_to_dev()
{
    cudaMemcpy(d_xC, h_xC, size_f, cudaMemcpyHostToDevice);
    cudaMemcpy(d_xE, h_xE, size_f, cudaMemcpyHostToDevice);
    cudaMemcpy(d_y, h_y, size_f, cudaMemcpyHostToDevice);
    cudaMemcpy(d_w, h_w, size_f, cudaMemcpyHostToDevice);
}

void copy_from_dev()
{
    cudaMemcpy(h_xC, d_xC, size_f, cudaMemcpyDeviceToHost);
    cudaMemcpy(h_xE, d_xE, size_f, cudaMemcpyDeviceToHost);    
    cudaMemcpy(h_y, d_y, size_f, cudaMemcpyDeviceToHost);
    cudaMemcpy(h_w, d_w, size_f, cudaMemcpyDeviceToHost);
}

void initial_conditions()
//set initial conditions for path parameters
{
    //xc 8000, xe 25000, y 100, w 0
    
    int i;
    for (i = 0; i < h_paths; ++i){
	h_xC[i]  = h_xC_init;
	h_xE[i]  = h_xE_init;
	h_y[i]  = h_y_init;
	h_w[i]  = h_w_init;
    }
       copy_to_dev();
}

void finish()
//free memory
{

    free(h_xC);
    free(h_xE);    
    free(h_y);
    free(h_w);
    
    curandDestroyGenerator(gen);
    cudaFree(d_xC);
    cudaFree(d_xE);    
    cudaFree(d_y);
    cudaFree(d_w);
    cudaFree(d_states);
    
}

int main(int argc, char **argv)
{
    if (argc < 2) {
        usage(argv);
        return -1;
    }

    parse_cla(argc, argv);

    prepare();
    
    initial_conditions();
   

    int i, j;

    printf("#t xC xE y w run\n");
    for (j = 0; j < h_paths; ++j)
	printf("%d %e %e %e %e %d\n", 0, h_xC[j], h_xE[j], h_y[j], h_w[j], j);

    for (i = 1; i <= h_years; i++) {
      run_traj<<<h_grid, h_block>>>(d_xC, d_xE, d_y, d_w, d_states);
      copy_from_dev();
      for (j = 0; j < h_paths; ++j)
	printf("%d %e %e %e %e %d\n", i, h_xC[j], h_xE[j], h_y[j], h_w[j], j);
    }

    finish();

    return 0;
}
